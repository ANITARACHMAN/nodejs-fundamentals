var express  = require('express');
var app = express();

var name = 'Anita'
var port = 2300
var email ='rachmananita12@gmail.com'

app.get('/', function(req,res) {
    res.json (
        {
            succes  : 200,
            name    : 'My name is ' + name,
            email   :`My email is ${email}`  
            
        }
    );
});

app.listen(process.env.PORT || port, function(){
    console.log('Your node js server is running on port ' + port );
});